import java.time.*;
import java.time.format.*;
import java.util.Locale;

public class Babysitter {

	public LocalTime	startTime;
	public LocalTime	bedTime;
	public LocalTime	endTime;

	// Using integers because we only use whole hours and whole dollar amounts.

	public Integer	pay;

	public Integer	beforeBedHours;
	public Integer	bedToMidnightHours;
	public Integer	afterMidnightHours;

	public final Integer beforeBedHourlyPay 	= 12;
	public final Integer bedToMidnightHourlyPay	=  8;
	public final Integer afterMidnightPay		= 16;
	
	//Note: tried to allow 5:00 PM instead of 05:00PM but all attempts failed (it wants that leading zero)

	public DateTimeFormatter fmt  = new DateTimeFormatterBuilder()
        .parseCaseInsensitive()
        .appendPattern( "hh:mma" )
        .toFormatter( Locale.ENGLISH );

	// Constructor with no params to instantiate an empty/default object for tests.
	// Null used instead of zero to indicate uninitialized or problem state.

	public Babysitter() {

		startTime = null;
		bedTime = null;
		endTime = null;

		pay = null;

		beforeBedHours = null;
		bedToMidnightHours = null;
		afterMidnightHours = null;
	}

	// Constructor with 3 string time params to use to instantiate an object with times for normal use.

	public Babysitter( String start, String bed, String end ) {

		setStartTime( start );
		setBedTime( bed );
		setEndTime( end );

		pay = 0;

		beforeBedHours = 0;
		bedToMidnightHours = 0;
		afterMidnightHours = 0;
	}

	//TODO: Might want a constructor that takes 3 LocalTimes instead of 3 Strings?

	// Assuming: startTime is always before bedTime
	//           bedTime is always before midnight
	//			 endTime is always after bedtime
	//			 BUT endTime may be be either on or AFTER midnight
	// These assumptions are based on the wording of the code kata.

	public void calculateSitterPay() {

		Integer startHour;
		Integer bedHour;
		Integer endHour;
		Integer midnightHour = 24;

		// Since we work with only whole hours, To make calculations simpler, treat all times as simple
		// integers 17 and higher, and treat midnight-4am as hours 24-28

		//TODO: if :mm of time is not :00, round up to next hour? Or just assume sitter does that?

		startHour = this.getStartTime().getHour();
		startHour = biasHoursAfterMidnight( startHour );

		bedHour = this.getBedTime().getHour();
		bedHour = biasHoursAfterMidnight( bedHour );

		endHour = this.getEndTime().getHour();
		endHour = biasHoursAfterMidnight( endHour );

		// now 5:00PM-4:00AM are 1700-2800 (just hours)

		if ( ( startHour <= bedHour ) && ( bedHour <= endHour ) ) {
			// then times are ok.

			this.beforeBedHours   = bedHour - startHour;

			if ( endHour <= midnightHour ) {
				this.bedToMidnightHours = endHour - bedHour;
				this.afterMidnightHours = 0;
			}
			else { // endHour > midnight
				this.bedToMidnightHours = midnightHour - bedHour;
				this.afterMidnightHours = endHour - midnightHour;
			}

			this.pay = this.beforeBedHours     * this.beforeBedHourlyPay
					 + this.bedToMidnightHours * this.bedToMidnightHourlyPay
					 + this.afterMidnightHours * this.afterMidnightPay;
		}
		else {
			System.err.println( "" );
			System.err.println( "Hours provided are not in chronological order:" );

			if ( startHour > bedHour ) {
				System.err.println( "  - Start hour is after bedtime hour." );
			}

			if ( bedHour > endHour ) {
				System.err.println( "  - Bedtime hour is after ending hour." );
			}
			System.err.println( "" );
			this.pay = null; //set pay to null to indicate error calculating.
		}
	}

	public Integer biasHoursAfterMidnight( Integer hour ) {
		if ( hour <= 4 ) hour = hour + 24;
		return hour;
	}

	// Setters
	//Overloaded versions of these methods so we can set the value with either a String or a LocalTime.

	public void setStartTime( String startTime ) {
		this.startTime = convertStringToLocalTime( startTime );
	}

	public void setStartTime( LocalTime start ) {
		this.startTime = start;
	}

	public void setBedTime( String bedTime ) {
		this.bedTime = convertStringToLocalTime( bedTime );
	}

	public void setBedTime( LocalTime bedTime ) {
		this.bedTime = bedTime;
	}

	public void setEndTime( String endTime ) {
		this.endTime = convertStringToLocalTime( endTime );
	}

	public void setEndTime( LocalTime endTime ) {
		this.endTime = endTime;
	}

	// Getters

	public LocalTime getStartTime() {
		return this.startTime;
	}

	public LocalTime getBedTime() {
		return this.bedTime;
	}

	public LocalTime getEndTime() {
		return  this.endTime;
	}

	//TODO: Create overloaded getters that return integers for hour too?


	public LocalTime convertStringToLocalTime( String time ) {

		LocalTime localtime = null;	// just to be safe.

		try {
			localtime = LocalTime.parse( time, fmt );

			if  ( !timeIsInValidRange( localtime ) ) {
				localtime = null;
			}
		}
		catch ( DateTimeParseException dtpe ) {
			localtime = null;
		}
		catch ( Exception e ) {
			localtime = null;
		}

		return localtime;
	}

	// Times must start no earlier than 5:00pm (17:00) and sitting can end no later than 4:00AM 
	// and it IS possible (though unlikely) that you might be called after midnight to sit.

	public Boolean timeIsInValidRange( LocalTime time ) {

		final int minStartTime = 17; // 17:00 or 5:00 PM
		final int minEndTime 	= 4; // 04:00 or 4:00 AM

		//TODO: Change to compare LocalTimes of 05:00PM and 04:00AM or just check .getMinutes()=0?
		//TODO: Move those time declarations from BabysitterTest.java object fields to Babysitter.Java?

		if ( ( time.getHour() >= minStartTime ) || ( time.getHour() <= minEndTime ) ) {
			return true;
		}
		else {
			return false;
		}
	}
}
