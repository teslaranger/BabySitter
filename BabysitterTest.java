import org.junit.*;
import static junit.framework.TestCase.*;

import java.time.*;
import java.time.format.*;
import java.util.Locale;

public class BabysitterTest {

	public LocalTime	tooEarly;
	public String		tooEarlyText = "04:00PM";

	public LocalTime	tooLate;
	public String		tooLateText  = "05:00AM";  // latest allowed time is 4:00 am; we only check hour so 5 is too late.

	public LocalTime	fivePM;
	public String		fivePMtext = "05:00PM";

	public LocalTime	fourAM;
	public String		fourAMtext = "04:00AM";

	public LocalTime	midnight;
	public String		midnightText = "12:00AM";

	public String		badTimeText = "13:00AM";

	public LocalTime	bedTime;
	public String		bedTimeText = "08:00PM";

	public LocalTime	endTime;
	public String		endTimeText = "04:00AM";

	public Babysitter	nullSit;
	public Babysitter threeGoodParams;

	public DateTimeFormatter fmt  = new DateTimeFormatterBuilder()
		.parseCaseInsensitive()
		.appendPattern("hh:mma")
		.toFormatter(Locale.ENGLISH);

	@Before
	public void setup() {
		tooEarly = LocalTime.parse(tooEarlyText, fmt);
		tooLate  = LocalTime.parse(tooLateText, fmt);
		fivePM   = LocalTime.parse(fivePMtext, fmt);
		fourAM   = LocalTime.parse(fourAMtext, fmt);
		midnight = LocalTime.parse(midnightText, fmt);
		bedTime  = LocalTime.parse(bedTimeText, fmt);
		endTime  = LocalTime.parse(endTimeText, fmt);

		nullSit = new Babysitter();  // constructor that inits times to null
		threeGoodParams = new Babysitter( fivePMtext, bedTimeText, endTimeText );

	}
	
	@After
	public void cleanup() {
		// clean up anything that needs cleaning, after tests run
	}

	// Tests for method timeIsInValidRange()

	@Test
	public void whenTimeIsBeforeValidTimeReturnFalse() {
		assertFalse(nullSit.timeIsInValidRange(tooEarly));
	}


	// Test convertStringToLocalTime menthod (and valid-range check) returns null if time
	// out of range or invalid.

	@Test
	public void whenTimeIsBefore5pmReturnNull() {
		assertNull(nullSit.convertStringToLocalTime(tooEarlyText));
	}
	
	@Test
	public void whenTimeIs4amOrLaterReturnNull() {
		assertNull(nullSit.convertStringToLocalTime(tooLateText));
	}
	
	@Test
	public void whenTimeis5pmReturn5pm() {
		LocalTime st = nullSit.convertStringToLocalTime(fivePMtext);
		assertEquals(fivePM, st);
	}

	@Test
	public void whenTimeisMidnightReturnMidnight() {
		LocalTime st = nullSit.convertStringToLocalTime(midnightText);
		assertEquals(midnight, st);
	}

	@Test
	public void whenTimeisNotAValidTimeReturnNull() {
		assertNull(nullSit.convertStringToLocalTime(badTimeText));
	}

	// Test Babysitter.setStartTime() & .getStartTime() using BabySitter null constructor

	@Test
	public void checkGetStartTimeReturnsNullFromNullConstructor() {
		assertNull(nullSit.getStartTime());
	}

	@Test
	public void whenStartTimeSentAsTextIsTooEarly() {
		nullSit.setStartTime(tooEarlyText);		// started as null, should STAY null...
		assertNull( nullSit.getStartTime() );
	}

	@Test
	public void whenStartTimeSentAsTextIsValidSetTime() {
		nullSit.setStartTime(fivePMtext);
		assertEquals( fivePM, nullSit.getStartTime() );
	}

	@Test
	public void checkStartTimeSentAsValidLocalTimeIsValidSetTime() {
		nullSit.setStartTime(fivePM);
		assertEquals( fivePM, nullSit.getStartTime() );
	}

	@Test
	public void checkStartTimeSetToNullWhenSentInvalidTime() {
		nullSit.setStartTime(badTimeText);
		assertNull(nullSit.getStartTime() );
	}

	// Test Babysitter.setBedTime() & .getBedTime() using BabySitter null constructor

	@Test
	public void checkGetBedTimeReturnsNullFromNullConstructor() {
		assertNull(nullSit.getBedTime());
	}

	@Test
	public void whenBedTimeSentAsTextIsTooEarly() {
		nullSit.setBedTime(tooEarlyText);		// started as null, should STAY null...
		assertNull( nullSit.getBedTime() );
	}

	@Test
	public void whenBedTimeSentAsTextIsValidSetTime() {
		nullSit.setBedTime(bedTimeText);
		assertEquals( bedTime, nullSit.getBedTime() );
	}

	@Test
	public void checkBedTimeSentAsValidLocalTimeIsValidSetTime() {
		nullSit.setBedTime(bedTime);
		assertEquals( bedTime, nullSit.getBedTime() );
	}

	@Test
	public void checkBedTimeSetToNullWhenSentInvalidTime() {
		nullSit.setBedTime(badTimeText);
		assertNull(nullSit.getBedTime() );
	}

	// Test Babysitter.setEndTime() & .getEndTime() using BabySitter null constructor

	@Test
	public void checkGetEndTimeReturnsNullFromNullConstructor() {
		assertNull(nullSit.getEndTime());
	}

	@Test
	public void whenEndTimeSentAsTextIsTooEarly() {
		nullSit.setEndTime(tooEarlyText);		// started as null, should STAY null...
		assertNull( nullSit.getEndTime() );
	}

	@Test
	public void whenEndTimeSentAsTextIsValidSetTime() {
		nullSit.setEndTime(endTimeText);
		assertEquals( endTime, nullSit.getEndTime() );
	}

	@Test
	public void checkEndTimeSentAsValidLocalTimeIsValidSetTime() {
		nullSit.setEndTime(endTime);
		assertEquals( endTime, nullSit.getEndTime() );
	}

	@Test
	public void checkEndTimeSetToNullWhenSentInvalidTime() {
		nullSit.setEndTime(badTimeText);
		assertNull(nullSit.getEndTime() );
	}

	// Test that three-param constructor (declared in Before method) gets all 3 params ok; check one param at a time

	@Test
	public void whenThreeArgumentConstructorIsGivenGoodStartTime() {
		assertEquals( fivePM, threeGoodParams.getStartTime() );
	}

	@Test
	public void whenThreeArgumentConstructorIsGivenGoodBedTime() {
		assertEquals( bedTime, threeGoodParams.getBedTime() );
	}

	@Test
	public void whenThreeArgumentConstructorIsGivenGoodEndTime() {
		assertEquals( endTime, threeGoodParams.getEndTime() );
	}

	@Test
	public void checkifThreeArgumentContructorCalculatedPayRight() {
		// 5pm start, 8pm bed, 4am end, so 2x12 ($36) + 4*8 ($32) + 4*16 ($64) = $132
		Integer rightPay = 132;  
		threeGoodParams.calculateSitterPay();

		System.out.println(" ");
		System.out.println("Start : " + threeGoodParams.getStartTime().format(fmt) );
		System.out.println("Bed   : " + threeGoodParams.getBedTime().format(fmt) );
		System.out.println("End   : " + threeGoodParams.getEndTime().format(fmt) );

		System.out.println("Pay   : $" + threeGoodParams.pay );

		System.out.println("Before Bed hours     :" + threeGoodParams.beforeBedHours );
		System.out.println("Bed to Midnight hours:" + threeGoodParams.bedToMidnightHours );
		System.out.println("After Midnight hours :" + threeGoodParams.afterMidnightHours );
		System.out.println(" ");

		assertEquals( rightPay, threeGoodParams.pay );
	}

	// Test that we handled getting time parameters out of order; should return pay value of null not zero.

	@Test
	public void checkifArgumentsNotInChronologicalOrderHandledBySettingPayToNull() {
		Babysitter bs = new Babysitter( "07:00PM", "05:00PM", "06:00PM");
		bs.calculateSitterPay();
		assertNull( bs.pay );
	}

	@Test
	public void checkToSeeIfArgumentsAreBackwardsAndWeGetBothErrorMessagesAndPayIsSetToNull() {
		Babysitter bs = new Babysitter( "07:00PM", "06:00PM", "05:00PM");
		bs.calculateSitterPay();
		assertNull( bs.pay );
	}
}
