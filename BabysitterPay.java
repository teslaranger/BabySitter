import java.time.*;
import java.time.format.*;
import java.util.Locale;

public class BabysitterPay {

	// takes 3 arguments;  start-time, bed-time, end-time, in format "HH:MM xM" e.g. "05:00 PM"
	public static void main( String[] args ) {

		Integer returnCode = null;

		if ( args.length != 3 ) {
				System.err.println( "Too many parameters; must be exactly 3." );
				usage();
				returnCode = 1;
			}
			else {
				Babysitter sitter = new Babysitter( args[0], args[1], args[2] );

				try {
					sitter.calculateSitterPay();
					if ( sitter.pay != null ) {
						reportSitterPay( sitter );
						returnCode = 0;
					}
				}
				catch ( Exception e ) {
					System.err.println( "Unexpected error calculating sitter pay: " + e.getMessage() );
					returnCode = 2;
				}
			}
		System.exit( returnCode );			// integer return code sent back to OS; 0= no error. 
	}

	
	private static void usage() {
		System.out.println("Usage: BabysitterPay start-time, bed-time, and end-time.");
		System.out.println("Time format: HH:MMAA, e.g. 05:00PM or 01:00AM.");
		System.out.println("Example: BabysitterPay 05:00PM 08:00PM 01:00AM");
	}

	public static void reportSitterPay( Babysitter bs ) {

		DateTimeFormatter fmt = new DateTimeFormatterBuilder()
				.parseCaseInsensitive()
				.appendPattern( "hh:mma" )
				.toFormatter( Locale.ENGLISH );

		System.out.println( "" );
		System.out.println( "SITTER PAY REPORT" );
		System.out.println( "Start   : " + bs.startTime.format(fmt) );
		System.out.println( "Bedtime : " + bs.bedTime.format(fmt) );
		System.out.println( "End     : " + bs.endTime.format(fmt) );
		System.out.println( "Pay     : $" + bs.pay );
		System.out.println( "" );
	}
}
