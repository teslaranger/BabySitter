Babysitter Kata for/from Pillar Technologies

Source:
https://github.com/PillarTechnology/kata-babysitter#babysitter-kata

======================================

Background

This kata simulates a babysitter working and getting paid for one night. The rules are pretty straight forward.

The babysitter:

-starts no earlier than 5:00PM
-leaves no later than 4:00AM

-gets paid $12/hour from start-time to bedtime
-gets paid $8/hour from bedtime to midnight
-gets paid $16/hour from midnight to end of job
-gets paid for full hours (no fractional hours)

Feature

As a babysitter
In order to get paid for 1 night of work
I want to calculate my nightly charge

======================================

IMPORTANT NOTES:

This kata has been fun and I've learned a lot. 

I've been learning TDD and JUnit while working on this kata, , so there may be more tests than necessary.
I am still going through the mentals shifts necessary to do TDD properly, I think...please bear that in mind.

I've always writen test methods for all the regular methods I write, just no formal TDD or anything like 
JUnit. (e.g if I had a method ABC(), I'd write ABCTest() to run tests on it.

Also, I haven't used Java much for the last couple of years so I'm a bit rusty...there's probably 
some bad coding practices here or there, as I clean off the rust.

I've been learning Git over the last few months in my present contracting gig. I still have a lot to learn 
but I am really enjoying Git; it is far superior to the other VC systems I've used over the years (CVS, PVCS, 
VSS, etc.)

======================================

ISSUES NOT MADE CLEAR BY THE KATA BACKGROUND AND MAY NOT COVERED BY THIS PROGRAM

- What happens if the kid is already in bed or being put to bed when the sitting starts?
- What happens if the sitting starts after midnight (emergency sitter) and the kids already in bed?
- What happens if the kid's bedtime is midnight or after?

The program assumes that bedtime is after the start hour, and bedtime is before midnight.
It might possibly work if starttime=bedtime or bedtime=midnight or even bedtime=midnight=endtime.
Hmmm...more tests to write for the to-do list.  Those may or may not have been written  by the time you 
read this.  I am continuing to work on the kata as I have time. Learning JUnit and TDD was high on my 
to-do list, so this is an excellent practice target.  I plan to go through some of your other katas as 
well.


======================================
INSTRUCTIONS

I've set this up (hopefully) so that you don't even need an IDE, just a JDK/JRE in order to compile and run 
the tests.

Instructions are below, mostly for Windows  I have included the JUnit 4.10 jar file with the code in the repo. 

I used Java 1.8, specifically: 

java version "1.8.0_162"
Java(TM) SE Runtime Environment (build 1.8.0_162-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.162-b12, mixed mode)

I do use the LocalTime class for parsing and manipulating times, so you will need to use Java 8 to run the 
tests.

Note: There may be *.class files in the git repo; I think I forgot to create a .gitignore file out to exclude them immediately,
so a few early commits might have them.

=====================================

REQUIREMENTS

Java 1.8 JDK installed on your system
junit4.10.jar (provided)


WINDOWS INSTRUCTIONS

Once you have cloned the repo using git:

* Run the setup.bat script ONCE to set your environment variables for JUnit to the use the JUnit jar file in the current directory.
* Run the compile.bat script to compile the code
* Run the test.bat script to run the tests

TO DO MANUAL TESTING:

* Issue the command 'java BabysitterPay 05:00PM 08:00PM 01:00AM' command or similar to run the program
Note: I have also created a Windows batch script named BabysitterPay.bat to run the java app, so you can just
enter something like:

   BabysitterPay 05:00PM 08:00PM 01:00AM

* Provide (respectively) the start, bedtime and end times of the sitting gig in HH:MMxx format where
HH and MM must both be two digits (use a leading zero if needed) and the xx is either AM or PM (should
be case insensitive.) I had some difficulty getting times formatted like 5:00PM or 5:00 PM to work with
the new time objects, so I ultimately stopped trying instead of wasting a lot of time on the issue. 


LINUX/UNIX/MAC OS X INSTRUCTIONS

I have started to create Bash shell scripts to run on at least MacOS X and perhaps Ubuntu Linux. 

THEY ARE NOT FINISHED YET, AND UNTESTED!

(And now I have to go see if you can do TDD on Bash  shell scripts and Windows batch scripts... that should 
be interesting!)

The Widows .bat scripts for compile and test are possibly pretty close to what you'd use on Unix, though you 
will need to change the :: shortcuts for a comment in Windows batch scripts to the Bash # and perhaps use 
the commented out commands with the classpath -cp switch. 

Other .bat scripts may not be, such as the setup.bat script. 
