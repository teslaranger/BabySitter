:: Show the environment variables before creating or changing some

Set J
Set CLASSPATH

:: Add Junit-related environment variables, add junit jar file to Java classpath

Set JUNIT_HOME=.
Set CLASSPATH=%CLASSPATH%;%JUNIT_HOME%\junit-4.10.jar

:: Show the environment variables after
Set J
Set CLASSPATH
